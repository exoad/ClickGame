# [ Credits ]

**Exoad**
Hello, my name is exoad, or Jack. This was the project I made for my 2020-2021 Final Project of my Java Class at GNSHS.
Check out my socials [here](https://linktr.ee/exoad).

### Everything else
[ Main Page ](https://exoad.github.io/exoad/)
[ GitHub Profile ](https://github.com/exoad)