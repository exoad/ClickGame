# [ Final Project ]
These are the files that I wrote for the project I had to turn in for my Final Project of my Java Class of 2020-2021 at GNSHS.

These files are final and will not recieve any further updates in order to retain their originality 

**Avaliable Files:**
- [Video Demonstration for Flipgrid](https://github.com/exoad/ClickGame/blob/master/Final/FinalProjectFlipGrid.mp4)
- [Main Class](https://github.com/exoad/ClickGame/blob/master/Final/Controller.java)
- [Driver](https://github.com/exoad/ClickGame/blob/master/Final/Collect.java)
