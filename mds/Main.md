# [ Clicker Game ]
This was a simple project created for my Java Class of 2020-2021 at GNSHS. 

### [ [Final](https://github.com/exoad/ClickGame/tree/master/Final) ]

If you want the original copy go to the files of `/Final/`

There you would find the proper "original" files

Note: This was the original project I created for 2020-2021 at GNSHS

### [ [Remake](https://github.com/exoad/ClickGame/tree/master/Remake) ]
This is an improvement of the program, check the `TODO` file for upcoming changes.

Remake changes will not effect the `/Final/` files, etc..

### [ [Deprecated](https://github.com/exoad/ClickGame/tree/master/Deprecated) ]
Older stuffs that were removed for X reasons

### [ [Others](https://github.com/exoad/ClickGame/tree/master/Others) ]
Others...
