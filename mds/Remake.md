# [ Remake ]
This is an improved/revised version of the original project I had to turn in. It aims to be much more "usable" and "enjoyable."

**Avaliable Files:**
- [Main Driver & Class](https://github.com/exoad/ClickGame/blob/master/Remake/ControllerR.java)
- [Utility: Help_Panel](https://github.com/exoad/ClickGame/tree/master/Remake/Extra)
- [TODO](https://github.com/exoad/ClickGame/blob/master/Others/TODO)
